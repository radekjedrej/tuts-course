(function (e) {
  var $content = $('div.content').hide();

  jQuery.fn.flash = function (speed, easing, callback) {
    $(this).slideDown(1000, function () {
      $(this).delay(200).slideUp(400);
    });
  };

  $('h1').on('click', function () {
    $content.flash();
  });

})();