(function (e) {

  var obj = {
    doIt: function (e) {
      console.log(this);
    }
  }

  // function doSomething(e) {
  //   e.preventDefault();
  //   console.log(e.target);
  //   console.log(this);
  // }

  const $button = $('a')
  $button.on('click', function (e) {
    obj.doIt.call(this);
    e.preventDefault();
  });

})();