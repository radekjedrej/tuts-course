(function (e) {

  $('div.content').hide();

  // You can save your own jquery animation speed by overiding fx.speeds._default
  $.fx.speeds.tortoise = 5000;

  $('h1').on('click', function () {
    if ($(this).next().is(':visible')) {
      $(this).next().slideUp('tortoise');
    } else {
      $(this).next().slideDown('tortoise');
    }
  })

})();