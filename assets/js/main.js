(function () {
  const $button = $('button');
  const $link = $('link');

  $button.click(function () {
    const $this = $(this);
    const $stylesheet = $(this).attr('data-href');

    $link.attr('href', $stylesheet);

    $this
      .siblings('button')
      .removeAttr('disabled');

    $this
      .attr('disabled', 'disabled');
  });

})();