(function () {

  var box = $('div.box');

  $.fn.FadeSlideToggle = function (speed, fn) {
    return $(this).animate({
      'height': 'toggle',
      'opacity': 'toggle'
    }, speed || 400, function () {
      $.isFunction(fn) && fn.call(this);
    });
  };

  $('button').on('click', function () {
    box.FadeSlideToggle(500);
  })

})();